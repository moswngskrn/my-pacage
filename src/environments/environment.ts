// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBUPIS9I16I7IaAMvDFgoeYLFfm4ZjnhQQ",
    authDomain: "mypackage-nu.firebaseapp.com",
    databaseURL: "https://mypackage-nu.firebaseio.com",
    projectId: "mypackage-nu",
    storageBucket: "mypackage-nu.appspot.com",
    messagingSenderId: "80428162190"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
