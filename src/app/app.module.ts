import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { SuperTabsModule } from '@ionic-super-tabs/angular';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicStorageModule } from '@ionic/storage';
import { Tab1PageModule } from './members/home/tab1/tab1.module';
import { Tab2PageModule } from './members/home/tab2/tab2.module';

import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireFunctionsModule} from 'angularfire2/functions';
import {AngularFireStorageModule} from 'angularfire2/storage';
import {environment} from '../environments/environment'

import { QRCodeModule } from 'angularx-qrcode';
import { QrcodePageModule } from './members/qrcode/qrcode.module';
import { AdminPackageInPageModule } from './members/admin-package-in/admin-package-in.module';
import { AdminPackageOutPageModule } from './members/admin-package-out/admin-package-out.module';
import { PackageService } from './services/package.service';
import { AuthenticationService } from './services/authentication.service';

import { SignaturePadModule } from 'angular2-signaturepad';
import { CameraPreview} from '@ionic-native/camera-preview/ngx';
import { ListPageModule } from './members/list/list.module';
import { DetailPageModule } from './members/detail/detail.module';

import { HttpClientModule } from '@angular/common/http';

import { Screenshot } from '@ionic-native/screenshot/ngx';
// import { QRScanner } from '@ionic-native/qr-scanner/ngx';

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule, 
    BrowserAnimationsModule,
    HttpClientModule,
    IonicModule.forRoot(), 
    AppRoutingModule,
    Tab1PageModule,
    Tab2PageModule,
    QRCodeModule,
    QrcodePageModule,
    AdminPackageInPageModule,
    AdminPackageOutPageModule,
    AngularFireFunctionsModule,
    AngularFireStorageModule,
    SignaturePadModule,
    ListPageModule,
    DetailPageModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot(),
    SuperTabsModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PackageService,
    AuthenticationService,
    CameraPreview,
    Screenshot,
    // QRScanner,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
