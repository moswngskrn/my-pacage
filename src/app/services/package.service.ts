import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase'
import { AngularFireStorage } from 'angularfire2/storage';
const TOKEN_KEY = 'userid';
@Injectable({
  providedIn: 'root'
})
export class PackageService {
  userId: string;
  imageUpload: any;
  pictureData: string;
  constructor(
    private firebaseDatabase: AngularFireDatabase,
    private storage: Storage,
  ) {
    this.userId = localStorage.getItem('userid');
  }


  setPictureData(picture) {
    this.pictureData = picture;
  }

  getPictureData() {
    return this.pictureData;
  }

  timestampToDate(timestamp) {
    const date = new Date(timestamp);
    const mt = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"]
    const year = date.getFullYear();
    const mount = (date.getMonth() + 1);
    const day = date.getDate();
    const h = date.getHours();
    const m = date.getSeconds();
    const s = date.getSeconds();
    return day + " " + mt[mount - 1] + " " + (year + 543) + " " + h + ":" + m;
  }

  readPackage(packageKey) {
    this.firebaseDatabase.database.ref('packages/' + packageKey).update({
      read: true
    })
  }

  getPackage(packageKey) {
    return new Promise((resolve, reject) => {
      try {
        this.firebaseDatabase.database.ref('package/' + this.userId + '/' + packageKey)
          .once('value', snapshot => {
            // console.log(snapshot.val())
            resolve({ ...snapshot.val(), timestamp_in: this.timestampToDate(snapshot.val().timestamp_in), timestamp_out: this.timestampToDate(snapshot.val().timestamp_out) })
          })
      } catch (error) {
        reject(error)
      }
    })

  }

  onValueChangeOut() {
    return this.firebaseDatabase.database.ref('packages')
      .orderByChild('timestamp_in');
  }

  onValueChangeIn() {
    return this.firebaseDatabase.database.ref('packages')
      .orderByChild('timestamp_in');
  }

  getPackageIn() {
    return new Promise((resolve, reject) => {
      try {
        console.log('get package in form', localStorage.getItem('uid'))

        this.firebaseDatabase.database.ref('packages')
          .orderByChild('uid').equalTo(localStorage.getItem('uid'))
          .once('value', snapshot => {
            const result = [];
            let data = snapshot.val();
            let con = {
              date: "",
              countNotRead: 0,
              read: false,
              allBox: []
            }
            let count = 1;
            let keySet = "";
            for (var key in data) {
              const date = new Date(data[key].timestamp_in);
              const year = date.getFullYear();
              const mount = (date.getMonth() + 1);
              const day = date.getDate();
              const k = (year * 10000 + mount * 100 + day).toString();
              let m = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"]
              if (keySet == "") {
                keySet = k;
                con["date"] = day + " " + m[mount - 1] + " " + (year + 543)
              }

              if (keySet != k) {
                result.push(con);
                con = {
                  date: "",
                  countNotRead: 0,
                  read: false,
                  allBox: []
                }
                count = 1;
                keySet = k;
                con["date"] = day + " " + m[mount - 1] + " " + (year + 543)
              }
              if (data[key].read == 0) {
                con['countNotRead']++;
              }
              con["allBox"].push({ ...data[key], key: key, date_in: this.timestampToDate(data[key].timestamp_in), date_out: this.timestampToDate(data[key].timestamp_out) })
              // console.log(k)
            }
            // console.log(con);
            if (con.allBox.length) {
              result.push(con);
            }
            // console.log(result)
            resolve(result)
          })
      } catch (error) {
        reject(error)
      }

    })

  }

  getPackageOut() {
    return new Promise((resolve, reject) => {
      try {
        console.log('get package out form', this.userId)
        this.firebaseDatabase.database.ref('package/' + this.userId)
          .orderByChild('timestamp_in')
          .once('value', snapshot => {
            const result = [];
            let data = snapshot.val();
            let con = {
              date: "",
              countNotRead: 0,
              read: false,
              box1: { image: "", more: 0, key: "", read: 0 },
              box2: { image: "", more: 0, key: "", read: 0 },
              box3: { image: "", more: 0, key: "", read: 0 },
              allBox: []
            }
            let count = 1;
            let keySet = "";
            for (var key in data) {
              if (data[key].status == "out") {
                const date = new Date(data[key].timestamp_in);
                const year = date.getFullYear();
                const mount = (date.getMonth() + 1);
                const day = date.getDate();
                const k = (year * 10000 + mount * 100 + day).toString();
                let m = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"]
                if (keySet == "") {
                  keySet = k;
                  con["date"] = day + " " + m[mount - 1] + " " + (year + 543)
                }

                if (keySet != k) {
                  if (con.allBox.length > 3) {
                    con.box3.more = con.allBox.length - 2;
                  }
                  result.push(con);
                  con = {
                    date: "",
                    countNotRead: 0,
                    read: false,
                    box1: { image: "", more: 0, key: "", read: 0 },
                    box2: { image: "", more: 0, key: "", read: 0 },
                    box3: { image: "", more: 0, key: "", read: 0 },
                    allBox: []
                  }
                  count = 1;
                  keySet = k;
                  con["date"] = day + " " + m[mount - 1] + " " + (year + 543)
                }
                if (count <= 3) {
                  con["box" + count] = { image: data[key].image_url, more: 0, key: key, read: data[key].read }
                  count++;
                }
                if (data[key].read == 0) {
                  con['countNotRead']++;
                }
                con["allBox"].push({ ...data[key], key: key, date_in: this.timestampToDate(data[key].timestamp_in), date_out: this.timestampToDate(data[key].timestamp_out) })
                // console.log(k)
              }

            }
            if (con.allBox.length > 3) {
              con.box3.more = con.allBox.length - 2;
            }
            // console.log(con);
            if (con.allBox.length) {
              result.push(con);
            }
            // console.log(result)
            resolve(result)
          })
      } catch (error) {
        reject(error)
      }

    })

  }


  searchRoom(keyword) {
    return this.firebaseDatabase.database.ref('room')
      .orderByChild('room').startAt(keyword).endAt(keyword + "\uf8ff")
  }

  getUserInRoom(room) {
    // this.firebaseDatabase.database.ref('rooms')
    // .orderByChild('room')
    // .equalTo("1311").on('child_added',t=>{
    //   console.log(t.val())
    // })
    console.log("room:", room)
    return this.firebaseDatabase.database.ref('rooms')
      .orderByChild('room')
      .equalTo(room)
  }


  addUser(room, username, password) {
    var userid = room + "-" + Date.now();
    var refRoom = this.firebaseDatabase.database.ref('rooms');
    this.firebaseDatabase.database.ref('users')
      .push({
        create_at: firebase.database.ServerValue.TIMESTAMP,
        userid: userid,
        password: password,
        room: room,
        username: username
      }).then(() => {
        this.firebaseDatabase.database.ref('role/' + userid).set({
          role: "user",
          userid: userid
        }).then(() => {
          refRoom.orderByChild('room').equalTo(room)
            .once('value', snap => {
              snap.forEach(sp => {
                sp.child('user').ref.push(
                  {
                    create_at: firebase.database.ServerValue.TIMESTAMP,
                    status: "on",
                    userid: userid
                  }
                ).then(() => {
                  snap.forEach(sp => {
                    sp.child('user').forEach(s => {
                      if (s.val().userid != userid) {
                        s.ref.update({ status: "off" })
                      }
                    })
                  })
                })
              })

            });
        })
      })
  }

  changeUser(room, userid) {
    var refRoom = this.firebaseDatabase.database.ref('rooms');

    refRoom.orderByChild('room').equalTo(room)
      .once('value', snap => {
        snap.forEach(sp => {
          sp.child('user').forEach(s => {
            if (s.val().userid != userid) {
              s.ref.update({ status: "off" })
            } else {
              s.ref.update({ status: "on" })
            }
          })
        })
      });
  }

  addRoom(room, username, password) {
    return new Promise((resolve, reject) => {
      try {
        var userid = room + "-" + Date.now();
        var refRoom = this.firebaseDatabase.database.ref('rooms');
        refRoom.orderByChild('room').equalTo(room)
          .once('value', snap => {
            if (!snap.val()) {
              refRoom.push({
                create_at: firebase.database.ServerValue.TIMESTAMP,
                room: room
              }).then(() => {
                refRoom.orderByChild('room').equalTo(room)
                  .once('value', snap => {
                    snap.forEach(sp => {
                      sp.child('user').ref.push({
                        status: "on",
                        userid: userid
                      })
                    })
                  });
                this.firebaseDatabase.database.ref('users')
                  .push({
                    create_at: firebase.database.ServerValue.TIMESTAMP,
                    userid: userid,
                    password: password,
                    room: room,
                    username: username
                  }).then(() => {
                    this.firebaseDatabase.database.ref('role/' + userid).set({
                      role: "user",
                      userid: userid
                    })
                    resolve(true)
                  })
              })
            } else {
              resolve(false)
            }
          })

      } catch (error) {
        reject(error)
      }
    })


  }

  setImageUpload(image) {
    this.imageUpload = image;
  }

  // preAddPackage(room, url) {
  //   return new Promise((resolve, reject) => {
  //     this.firebaseDatabase.database.ref('user_data').orderByChild('room').once('value', list => {
  //       let lisPomise = []
  //       for (var key in list.val()) {
  //         lisPomise.push(this.addPackage(room, url, list.val().uid))
  //       }
  //       Promise.all(lisPomise).then()
  //     })
  //   })

  // }

  preAddPackage(room, url, uid, packageId) {
    return new Promise((resolve, reject) => {
      let ref = this.firebaseDatabase.database.ref('packages').push();
      ref.set({
        room: room,
        packageId: packageId,
        image: url,
        pagekageId: firebase.database.ServerValue.TIMESTAMP,
        read: false,
        signature: "",
        status: "in",
        timestamp_in: firebase.database.ServerValue.TIMESTAMP,
        timestamp_out: "",
        uid: uid,
        id: ref.key
      }).then(() => {
        resolve()
      }, error => {
        reject(error)
      })
    })
  }
  addPackage(room, url) {
    return new Promise((resolve, reject) => {
      let toDay = new Date(Date.now());
      let toDayString = toDay.getFullYear().toString() + toDay.getMonth().toString() + toDay.getDate().toString();
      this.firebaseDatabase.database.ref('package_count/' + toDayString)
        .once('value', (snap) => {
          console.log(snap.val())
          var packageId = toDayString + "-" + ((snap.val() | 0) + 1);
          this.firebaseDatabase.database.ref('user_data').orderByChild('room').equalTo(room).once('value', list => {
            let lisPomise = []
            for (var key in list.val()) {
              if (list.val()[key].status == "on") {
                lisPomise.push(this.preAddPackage(room, url, list.val()[key].uid, packageId))
              }
            }
            Promise.all(lisPomise).then(() => {
              snap.ref.set((snap.val() | 0) + 1).then(() => {
                resolve(packageId)
              }).catch(error => {
                reject(error)
              })
            }, error => {
              reject(error);
            })
          })
        }, error => {
          reject(error)
        })


      // var imageRef = this.firebaseStorage.ref(room + "/" + Date.now() + ".jpg")
      // var imageTask = imageRef.putString(image, 'data_url');
      // console.log("uploading")
      // imageTask.then((snapshot) => {
      //   console.log("upload ok")
      //   snapshot.ref.getDownloadURL().then(DATA_URL => {
      //     let toDay = new Date(Date.now());
      //     let toDayString = toDay.getFullYear().toString() + toDay.getMonth().toString() + toDay.getDate().toString();
      //     console.log(toDayString)

      //   }, error => {
      //     reject(error)
      //   })
      // }, error => {
      //   reject(error)
      // })




      // console.log(room, image)
      // let fileRef = firebase.storage().ref(room + "/" + Date.now() + ".jpg");
      // let uploadTask = fileRef.putString(image, 'data_url');
      // uploadTask.on('state_changed', (_snapshot: any) => {

      // }, _error => {
      //   reject(_error)
      // }, () => {
      //   uploadTask.snapshot.ref.getDownloadURL().then(DATA_URL => {
      //     let toDay = new Date(Date.now());
      //     let toDayString = toDay.getFullYear().toString() + toDay.getMonth().toString() + toDay.getDate().toString();
      //     console.log(toDayString)
      //     this.firebaseDatabase.database.ref('package_count/' + toDayString)
      //       .on('value', (snap) => {
      //         this.firebaseDatabase.database.ref('packages').push({
      //           room: room,
      //           packageId: toDayString + "-" + ((snap.val() | 0) + 1),
      //           image: DATA_URL,
      //           pagekageId: firebase.database.ServerValue.TIMESTAMP,
      //           read: false,
      //           signature: "",
      //           status: "in",
      //           timestamp_in: firebase.database.ServerValue.TIMESTAMP,
      //           timestamp_out: ""
      //         }).then(() => {
      //           snap.ref.set((snap.val() | 0) + 1)
      //           resolve(true)
      //         }).catch(error => {
      //           reject(error)
      //         })
      //       })
      //     // let ref = this.firebaseDatabase.database.ref('rooms');
      //     // let key = ref.push().key;

      //   }, error => {
      //     reject(error)
      //   })
      // })
    })
  }

  uploadFile() {
    return new Promise((resolve, reject) => {
      let fileRef = firebase.storage().ref("image/" + Date.now() + ".jpg");
      let uploadTask = fileRef.putString(this.pictureData, 'data_url');
      uploadTask.on('state_changed', (_snapshot: any) => {
        console.log(
          "snapshot progess " +
          (_snapshot.bytesTransferred / _snapshot.totalBytes) * 100
        );
      }, _error => {
        reject(_error)
      }, () => {
        uploadTask.snapshot.ref.getDownloadURL().then(DATA_URL => {
          resolve(DATA_URL)
        }, error => {
          reject(error)
        })
      })
    })
  }

  uploadFile2(image) {
    return new Promise((resolve, reject) => {
      let fileRef = firebase.storage().ref("signature/" + Date.now() + ".jpg");
      let uploadTask = fileRef.putString(image, 'data_url');
      uploadTask.on('state_changed', (_snapshot: any) => {
        console.log(
          "snapshot progess " +
          (_snapshot.bytesTransferred / _snapshot.totalBytes) * 100
        );
      }, _error => {
        reject(_error)
      }, () => {
        uploadTask.snapshot.ref.getDownloadURL().then(DATA_URL => {
          resolve(DATA_URL)
        }, error => {
          reject(error)
        })
      })
    })
  }

  getOnPackage(packageId) {

  }


  getListPackageByRoom(room) {
    return new Promise((resolve, reject) => {
      this.firebaseDatabase.database.ref('packages').orderByChild('room').equalTo(room)
        .once('value', snap => {
          let data = snap.val();
          let list = new Object;
          for (var key in data) {
            if(data[key].status=='in'){
              list[data[key].packageId] = {
                select:false,
                key:data[key].packageId,
                image:data[key].image,
                package_id:data[key].packageId,
                date:this.timestampToDate(data[key].timestamp_in)
              }
            }
            
          }
          let listOK = []
          for(var key in list){
            listOK.push(list[key])
          }
          resolve(listOK)
        },error=>{
          reject(error)
        })
    })

  }


  prePackageOut(packageId,signature){
    console.log(packageId)
    return new Promise((resolve,reject)=>{
      this.firebaseDatabase.database.ref('packages')
      .orderByChild('packageId')
      .equalTo(packageId)
      .once('value',(snapshot)=>{
        let data = snapshot.val();
        console.log(data)
        let updateObj = {}
        for(var key in data){
          updateObj['packages/'+key+'/signature'] =signature;
          updateObj['packages/'+key+'/status'] ="out";
          updateObj['packages/'+key+'/timestamp_out'] =firebase.database.ServerValue.TIMESTAMP
        }
        this.firebaseDatabase.database.ref().update(updateObj).then(()=>{
          resolve()
        },err=>{
          reject(err)
        })
      },error=>{
        reject(error)
      })
    })
  }
  packageOut(list,signature){
    console.log(list)
    let listPromise = []
    for(var k in list){
      console.log(list[k])
      listPromise.push(this.prePackageOut(list[k].package_id,signature))
    }
    return Promise.all(listPromise)
  }
}