import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireFunctions } from 'angularfire2/functions';
import { AngularFireAuth } from 'angularfire2/auth';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
const TOKEN_KEY = 'userid';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authenticationState = new BehaviorSubject(false);
  checkLoginState = new BehaviorSubject(false);
  user = null;
  constructor(
    private storage: Storage,
    private platform: Platform,
    private firebaseDatabase: AngularFireDatabase,
    private angularFireFunctions: AngularFireFunctions,
    private angularFireAuth: AngularFireAuth,
    private splashScreen: SplashScreen,
  ) {
    this.checkLogin()
  }



  create(username, password) {
    return new Promise((resolve, reject) => {
      this.angularFireFunctions.functions.httpsCallable('createUser')({ username: username, password: password })
        .then(data => {
          resolve(data)
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  // test() {
  //   this.angularFireFunctions.functions.httpsCallable('signIn')({ username: "1311", password: "123456" })
  //     .then(data => {
  //       if (data.data.token) {
  //         this.angularFireAuth.auth.signInWithCustomToken(data.data.token)
  //           .then(user => {
  //             console.log(user.user.uid)
  //           })
  //       }

  //     })
  //     .catch(error => {
  //       console.log(error)
  //     })
  // }

  checkLogin() {
    console.log("check login")
    this.angularFireAuth.authState.subscribe(user => {
      if (user) {
        console.log("user login:", user.uid)
        this.firebaseDatabase.database.ref('role/' + user.uid)
          .on('value', data => {
            if (data.val() == "admin") {
              localStorage.setItem('role', data.val());
              localStorage.setItem('uid', user.uid);
              this.checkLoginState.next(true)
              this.authenticationState.next(true);
            } else {
              this.firebaseDatabase.database.ref('user_data').child(user.uid).once('value', u => {
                if (u.val().room) {
                  localStorage.setItem('room', u.val().room);
                  localStorage.setItem('role', data.val());
                  localStorage.setItem('uid', user.uid);
                  this.authenticationState.next(true);
                }
                this.checkLoginState.next(true)
              }, error => {
                this.checkLoginState.next(true)
              })
            }
          })
      } else {
        console.log("don't have user")
        this.checkLoginState.next(true)
        // this.logout();
      }
    })
  }

  login(username, password) {
    return new Promise((resolve, reject) => {
      try {
        console.log("authentication login()")
        this.angularFireFunctions.functions.httpsCallable('signIn')({ username: username, password: password })
          .then(data => {
            console.log("authentication login():signIn success")
            if (data.data.token) {
              console.log("authentication login():have token")
              this.angularFireAuth.auth.signInWithCustomToken(data.data.token)
                .then(user => {
                  resolve(user)
                })
            } else {
              console.log("authentication login():error token,", data)
              reject()
            }
          })
          .catch(error => {
            console.log("authentication login():", error)
            reject(error)
          })
      } catch (error) {
        reject(error)
      }
    })

  }
  logout() {
    return this.angularFireAuth.auth.signOut().then(() => {
      localStorage.clear()
      this.authenticationState.next(false);
      this.checkLoginState.next(false);
    })
  }
  isAuthenticated() {
    return this.authenticationState.value;
  }
}
