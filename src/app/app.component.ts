import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService,
    private router: Router,
    private firebase: AngularFireDatabase,
    private navCtlr:NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.authenticationService.checkLoginState.subscribe(checkLoginFinish=>{
        if(checkLoginFinish){
          console.log("check login finish")
          this.authenticationService.authenticationState.subscribe(isUserLogined=>{
            if(isUserLogined){
              console.log("user logined and check user lore")
              if(localStorage.getItem('role') == 'admin'){
                console.log("user is admin and go to admin page");
                // this.router.navigate(['members', 'admin-home']);
                this.navCtlr.navigateRoot('members/admin-home');
                this.splashScreen.hide();
              }else{
                console.log("user is not admin and go to member page")
                // this.router.navigate(['members', 'home']);
                this.navCtlr.navigateRoot('members/home');
                this.splashScreen.hide();
              }
            }else{
              console.log("user do not login and go to login page")
              // this.router.navigate(['login']);
              this.navCtlr.navigateRoot('login');
              this.splashScreen.hide();
            }
          })
        }
      })
    });
  }
}
