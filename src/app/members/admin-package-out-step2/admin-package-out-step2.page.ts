import { Component, OnInit, NgZone } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { PackageService } from 'src/app/services/package.service';
import { NavController } from '@ionic/angular';
// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
@Component({
  selector: 'app-admin-package-out-step2',
  templateUrl: './admin-package-out-step2.page.html',
  styleUrls: ['./admin-package-out-step2.page.scss'],
})
export class AdminPackageOutStep2Page implements OnInit {
  countSelect: number = 0;
  selectList: any = []
  room: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private packageService: PackageService,
    private ngZone: NgZone,
    private navCtlr: NavController,
    // private qrScanner: QRScanner
  ) {
    this.route.queryParams.subscribe(params => {
      if (params && params.room) {
        this.room = params.room;
        console.log(params.room)
        this.packageService.getListPackageByRoom(params.room).then(data => {
          console.log(data)
          this.selectList = data;
        })
      }
    });
  }

  ngOnInit() {
    this.scan();
  }

  select(key) {
    let have = false;
    for (var k in this.selectList) {
      if (this.selectList[k].key == key) {
        have = true;
        if (this.selectList[k].select) {
          this.countSelect--;
        } else {
          this.countSelect++;
        }
        this.selectList[k].select = !this.selectList[k].select;
      }
    }
    if (have) {
      console.log(key)
    } else {
      console.log("not key", key)
    }
  }
  next() {
    if (this.countSelect) {
      console.log("next")
      let listDataSelect = []
      for (var k in this.selectList) {
        if (this.selectList[k].select) {
          // console.log(this.selectList[k])
          listDataSelect.push(this.selectList[k])
        }
      }
      console.log("before", listDataSelect)
      let navigationExtras: NavigationExtras = {
        queryParams: {
          special: JSON.stringify({ data: listDataSelect })
        }
      };
      this.router.navigate(['', 'members', 'admin-package-out-step3'], navigationExtras);

    }
  }
  back() {
    this.ngZone.run(() => {
      this.navCtlr.navigateBack('members/admin-package-out')
    })
  }


  scan() {
    // this.qrScanner.prepare()
    //   .then((status: QRScannerStatus) => {
    //     if (status.authorized) {
    //       // camera permission was granted


    //       // start scanning
    //       let scanSub = this.qrScanner.scan().subscribe((text: string) => {
    //         console.log('Scanned something', text);

    //         // this.qrScanner.hide(); // hide camera preview
    //         // scanSub.unsubscribe(); // stop scanning
    //       });

    //     } else if (status.denied) {
    //       // camera permission was permanently denied
    //       // you must use QRScanner.openSettings() method to guide the user to the settings page
    //       // then they can grant the permission from there
    //     } else {
    //       // permission was denied, but not permanently. You can ask for permission again at a later time.
    //     }
    //   })
    //   .catch((e: any) => console.log('Error is', e));
  }
}
