import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPackageOutStep2Page } from './admin-package-out-step2.page';

describe('AdminPackageOutStep2Page', () => {
  let component: AdminPackageOutStep2Page;
  let fixture: ComponentFixture<AdminPackageOutStep2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPackageOutStep2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPackageOutStep2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
