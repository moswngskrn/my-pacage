import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminPackageOutStep2Page } from './admin-package-out-step2.page';

const routes: Routes = [
  {
    path: '',
    component: AdminPackageOutStep2Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminPackageOutStep2Page]
})
export class AdminPackageOutStep2PageModule {}
