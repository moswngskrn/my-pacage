import { Component, OnInit, NgZone } from '@angular/core';
import { PackageService } from 'src/app/services/package.service';
import { Router, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-admin-package-out',
  templateUrl: './admin-package-out.page.html',
  styleUrls: ['./admin-package-out.page.scss'],
})
export class AdminPackageOutPage implements OnInit {
  result: any = []
  noResult: boolean = true;
  constructor(
    private packageService: PackageService,
    private router: Router,
    private navCtlr:NavController,
    private ngZone:NgZone
  ) { }

  ngOnInit() {
  }


  onInputTime(value) {
    console.log(value)
    if (value) {
      this.packageService.searchRoom(value).once('value', snapshot => {
        let data = snapshot.val();
        this.ngZone.run(() => {
          this.result = [];
          this.noResult = false;
          for (var key in data) {
            this.noResult = true;
            this.result.push({
              ...data[key],
              key: key
            })
          }
        })

      })
    } else {
      this.result = [];
      this.noResult = false;
    }

  }

  open(room) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        room:room
      }
    };
    this.router.navigate(['', 'members', 'admin-package-out-step2'], navigationExtras);

  }
  back(){
    this.ngZone.run(()=>{
      this.navCtlr.navigateBack('members/admin-home')
    })
  }

}

