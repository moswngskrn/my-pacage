import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPackageOutPage } from './admin-package-out.page';

describe('AdminPackageOutPage', () => {
  let component: AdminPackageOutPage;
  let fixture: ComponentFixture<AdminPackageOutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPackageOutPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPackageOutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
