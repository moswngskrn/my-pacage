import { Component, OnInit, NgZone } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { PackageService } from 'src/app/services/package.service';
import { HttpClient } from '@angular/common/http';
import * as firebase from 'firebase';
@Component({
  selector: 'app-admin-package-in-step2',
  templateUrl: './admin-package-in-step2.page.html',
  styleUrls: ['./admin-package-in-step2.page.scss'],
})
export class AdminPackageInStep2Page implements OnInit {
  room: string;
  picture: string;
  url: string;
  packageNumber: string;
  date: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public loadingController: LoadingController,
    private packageService: PackageService,
    public toastController: ToastController,
    private ngZone:NgZone,
    private navCtlr:NavController
  ) {
    // this.route.queryParams.subscribe(params => {
    //   if (params && params.picture) {
    //     this.picture = params.picture
    //     this.url = params.url
    //   }
    // })
    this.picture = this.packageService.getPictureData();
  }

  ngOnInit() {
  }

  async save() {
    if(!this.room) return;
    const loading = await this.loadingController.create({
      spinner:"bubbles",
      cssClass:"loading",
      mode:'ios'
    });
    await loading.present();
    this.packageService.uploadFile().then(url => {
      this.packageService.addPackage(this.room, url).then(packageid => {
        loading.dismiss();
        let navigationExtras: NavigationExtras = {
          queryParams: {
            special: JSON.stringify({ room: this.room, picture: this.picture, packageNumber: packageid, date: this.date })
          }
        };
        this.router.navigate(['', 'members', 'admin-package-in-step3'], navigationExtras);
      })
    }, error => {
      loading.dismiss();
      this.presentToast("เกิดข้อผิดพลาด")
    })
  }

  async presentToast(ms) {
    const toast = await this.toastController.create({
      message: ms,
      duration: 2000
    });
    toast.present();
  }


  back(){
    this.ngZone.run(()=>{
      this.navCtlr.navigateBack('members/admin-package-in')
    })
  }
}
