import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPackageInStep2Page } from './admin-package-in-step2.page';

describe('AdminPackageInStep2Page', () => {
  let component: AdminPackageInStep2Page;
  let fixture: ComponentFixture<AdminPackageInStep2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPackageInStep2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPackageInStep2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
