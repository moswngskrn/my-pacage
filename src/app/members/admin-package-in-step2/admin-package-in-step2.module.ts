import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminPackageInStep2Page } from './admin-package-in-step2.page';

const routes: Routes = [
  {
    path: '',
    component: AdminPackageInStep2Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminPackageInStep2Page]
})
export class AdminPackageInStep2PageModule {}
