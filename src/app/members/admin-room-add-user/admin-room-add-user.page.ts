import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PackageService } from 'src/app/services/package.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-admin-room-add-user',
  templateUrl: './admin-room-add-user.page.html',
  styleUrls: ['./admin-room-add-user.page.scss'],
})
export class AdminRoomAddUserPage implements OnInit {

  room:string;
  username:string;
  password:string;
  constructor(
    private route: ActivatedRoute,
    private packageServiec: PackageService,
    private firebaseDatabase: AngularFireDatabase,
    private router:Router,
    private navCtrl:NavController
  ) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        var data = JSON.parse(params.special);
        console.log(data)
        this.room = data.room;
      }
    })
    
  }

  ngOnInit() {
  }
  save(){
    if(this.room&&this.username&&this.password){
      console.log("save")
      this.packageServiec.addUser(this.room,this.username,this.password),
      this.navCtrl.navigateBack('members/admin-room-detail');
    }
  }

}
