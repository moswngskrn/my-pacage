import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminRoomAddUserPage } from './admin-room-add-user.page';

const routes: Routes = [
  {
    path: '',
    component: AdminRoomAddUserPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminRoomAddUserPage]
})
export class AdminRoomAddUserPageModule {}
