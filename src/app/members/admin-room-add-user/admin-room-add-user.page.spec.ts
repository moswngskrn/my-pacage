import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRoomAddUserPage } from './admin-room-add-user.page';

describe('AdminRoomAddUserPage', () => {
  let component: AdminRoomAddUserPage;
  let fixture: ComponentFixture<AdminRoomAddUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRoomAddUserPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRoomAddUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
