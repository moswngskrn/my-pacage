import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PackageService } from 'src/app/services/package.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { NavController, LoadingController, ToastController, AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-admin-room-add',
  templateUrl: './admin-room-add.page.html',
  styleUrls: ['./admin-room-add.page.scss'],
})
export class AdminRoomAddPage implements OnInit {

  room: string;
  username: string;
  password: string;

  constructor(
    private route: ActivatedRoute,
    private packageServiec: PackageService,
    private firebaseDatabase: AngularFireDatabase,
    private router: Router,
    private navCtrl: NavController,
    private authService: AuthenticationService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertController: AlertController
  ) {

    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        var data = JSON.parse(params.special);
        console.log(data)
        this.room = data.room;
      }
    })
  }

  ngOnInit() {
  }

  async save() {
    const loading = await this.loadingController.create({
      message: 'กำลังอัพโหลดข้อมูล',
    });
    await loading.present();
    if (this.room && this.username && this.password) {
      console.log("save")
      this.authService.create(this.room, this.password).then((data: any) => {
        console.log(data)
        if (data.data.error) {
          loading.dismiss();
          this.presentAlert('ไม่สามารถสร้างข้อมูลผู้ใช้ได้ มีข้อมูลผู้ใช้งานนี้แล้ว')
        } else {
          this.firebaseDatabase.database.ref('user_data/'+data.data.uid).set({
            uid:data.data.uid,
            name:this.username,
            room:this.room,
            status:"on"
          }).then(()=>{
            
            this.firebaseDatabase.database.ref('room/'+this.room).set({
              room:this.room,
              create_at:Date.now()
            }).then(()=>{
              loading.dismiss();
              this.presentToast("เพิ่มข้อมูลผู้ใช้เรียบร้อยแล้ว")
            },error=>{
              loading.dismiss();
              this.presentAlert('การลงทะเบียนไม่สมบูรณ์โปรดเปลี่ยนรหัสผ่าน')
            })
          },error=>{
            loading.dismiss();
            this.presentAlert('ไม่สามารถสร้างข้อมูลผู้ใช้ได้โปรดเปลี่ยนรหัสผ่าน')
          })
          
        }


      }, error => {
        loading.dismiss();
        this.presentAlert('ไม่สามารถสร้างข้อมูลผู้ใช้ได้โปรดลองใหม่อีกครั้ง')
        console.log(error)

      })
      // this.packageServiec.addRoom(this.room,this.username,this.password).then(success=>{
      //   if(success){
      //     console.log("add room succes")
      //     this.navCtrl.navigateBack('members/admin-room');
      //   }else{
      //     console.log("have room")
      //   }
      // })
    }
  }

  async presentToast(ms) {
    const toast = await this.toastController.create({
      message: ms,
      duration: 2000
    });
    toast.present();
  }
  async presentAlert(m) {
    const alert = await this.alertController.create({
      header: 'เกิดข้อผิดพลาด',
      message: m,
      buttons: ['ตกลง']
    });

    await alert.present();
  }

}