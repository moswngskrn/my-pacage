import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminRoomAddPage } from './admin-room-add.page';

const routes: Routes = [
  {
    path: '',
    component: AdminRoomAddPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminRoomAddPage]
})
export class AdminRoomAddPageModule {}
