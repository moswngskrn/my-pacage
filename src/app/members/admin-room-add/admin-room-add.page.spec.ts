import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRoomAddPage } from './admin-room-add.page';

describe('AdminRoomAddPage', () => {
  let component: AdminRoomAddPage;
  let fixture: ComponentFixture<AdminRoomAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRoomAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRoomAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
