import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRoomPage } from './admin-room.page';

describe('AdminRoomPage', () => {
  let component: AdminRoomPage;
  let fixture: ComponentFixture<AdminRoomPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRoomPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRoomPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
