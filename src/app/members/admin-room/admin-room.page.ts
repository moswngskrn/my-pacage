import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { PackageService } from 'src/app/services/package.service';

@Component({
  selector: 'app-admin-room',
  templateUrl: './admin-room.page.html',
  styleUrls: ['./admin-room.page.scss'],
})
export class AdminRoomPage implements OnInit {

  keySearch:string;
  result:any=[]
  constructor(
    private packageService:PackageService,
    private router:Router
  ) { }

  ngOnInit() {
  }

  onInputTime(value){
    // console.log(value)
    this.keySearch=value;
    if(value){
      this.packageService.searchRoom(value).once('value',snapshot=>{
        let data=snapshot.val();
        this.result=[];
        for(var key in data){
          this.result.push({
            ...data[key],
            key:key
          })
        }
      })
    }else{
      this.result=[];
    }
    
  }

  open(room){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify({room:room})
      }
    };
    this.router.navigate(['','members','admin-room-detail'], navigationExtras);
  
  }
}
