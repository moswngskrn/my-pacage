import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPackageInPage } from './admin-package-in.page';

describe('AdminPackageInPage', () => {
  let component: AdminPackageInPage;
  let fixture: ComponentFixture<AdminPackageInPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPackageInPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPackageInPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
