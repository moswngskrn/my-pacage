import { Component, OnInit, NgZone } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions, CameraPreviewDimensions } from '@ionic-native/camera-preview/ngx';
import * as firebase from 'firebase';
import { PackageService } from 'src/app/services/package.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-admin-package-in',
  templateUrl: './admin-package-in.page.html',
  styleUrls: ['./admin-package-in.page.scss'],
})
export class AdminPackageInPage implements OnInit {
  picture: string;
  constructor(
    private router: Router,
    private cameraPreview: CameraPreview,
    private packageService: PackageService,
    private navCtlr:NavController,
    private ngZone:NgZone
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.startCamera();
  }
  ionViewWillLeave() {
    this.stopCamera();
  }
  cap() {
    // this.navCtlr.navigateRoot('members/admin-package-in-step2')
    // this.navCtlr.
    // this.router.navigate(['', 'members', 'admin-package-in-step2']);
    const pictureOpts: CameraPreviewPictureOptions = {
      width: 1280,
      height: 1280,
      quality: 85
    }
    this.cameraPreview.takePicture(pictureOpts).then((imageData) => {
      this.picture = 'data:image/jpg;base64,' + imageData;

      this.packageService.setPictureData(this.picture);
      this.router.navigate(['', 'members', 'admin-package-in-step2']);
    }, (err) => {
      console.log(err);
    });


  }
  startCamera() {
    const cameraPreviewOpts: CameraPreviewOptions = {
      x: 0,
      y: 50,
      width: window.screen.width,
      height: window.screen.height,
      camera: 'rear',
      tapPhoto: true,
      previewDrag: true,
      toBack: true,
      alpha: 1
    }

    try {
      this.cameraPreview.startCamera(cameraPreviewOpts).then(
        (res) => {
          console.log(res)
        },
        (err) => {
          console.log(err)
        });
    } catch (error) {
    }
  }

  stopCamera() {
    this.cameraPreview.stopCamera();
  }


  back(){
    this.ngZone.run(()=>{
      this.navCtlr.navigateBack('members/admin-home')
    })
  }
}
