import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DetailPage } from '../detail/detail.page';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
  ) { }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }

  async detail(){
    const modal = await this.modalCtrl.create({
      component:DetailPage,
      componentProps:{}
    })
    modal.present();
  }
}
