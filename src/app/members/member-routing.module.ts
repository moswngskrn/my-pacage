import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'search', loadChildren: './search/search.module#SearchPageModule' },
  { path: 'qrcode', loadChildren: './qrcode/qrcode.module#QrcodePageModule' },
  { path: 'setting', loadChildren: './setting/setting.module#SettingPageModule' },
  { path: 'detail', loadChildren: './detail/detail.module#DetailPageModule' },
  { path: 'list', loadChildren: './list/list.module#ListPageModule' },
  { path: 'tab1', loadChildren: './home/tab1/tab1.module#Tab1PageModule' },
  { path: 'tab2', loadChildren: './home/tab2/tab2.module#Tab2PageModule' },
  { path: 'admin-home', loadChildren: './admin-home/admin-home.module#AdminHomePageModule' },
  { path: 'admin-package-in', loadChildren: './admin-package-in/admin-package-in.module#AdminPackageInPageModule' },
  { path: 'admin-package-out', loadChildren: './admin-package-out/admin-package-out.module#AdminPackageOutPageModule' },
  { path: 'admin-package-in-step2', loadChildren: './admin-package-in-step2/admin-package-in-step2.module#AdminPackageInStep2PageModule' },
  { path: 'admin-package-in-step3', loadChildren: './admin-package-in-step3/admin-package-in-step3.module#AdminPackageInStep3PageModule' },
  { path: 'admin-package-out-step2', loadChildren: './admin-package-out-step2/admin-package-out-step2.module#AdminPackageOutStep2PageModule' },
  { path: 'admin-package-out-step', loadChildren: './admin-package-out-step/admin-package-out-step.module#AdminPackageOutStepPageModule' },
  { path: 'admin-package-out-step3', loadChildren: './admin-package-out-step3/admin-package-out-step3.module#AdminPackageOutStep3PageModule' },
  { path: 'admin-room', loadChildren: './admin-room/admin-room.module#AdminRoomPageModule' },
  { path: 'admin-room-add', loadChildren: './admin-room-add/admin-room-add.module#AdminRoomAddPageModule' },
  { path: 'admin-room-detail', loadChildren: './admin-room-detail/admin-room-detail.module#AdminRoomDetailPageModule' },
  { path: 'admin-room-add-user', loadChildren: './admin-room-add-user/admin-room-add-user.module#AdminRoomAddUserPageModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class MemberRoutingModule { }
