import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPackageOutStepPage } from './admin-package-out-step.page';

describe('AdminPackageOutStepPage', () => {
  let component: AdminPackageOutStepPage;
  let fixture: ComponentFixture<AdminPackageOutStepPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPackageOutStepPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPackageOutStepPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
