import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminPackageOutStepPage } from './admin-package-out-step.page';

const routes: Routes = [
  {
    path: '',
    component: AdminPackageOutStepPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminPackageOutStepPage]
})
export class AdminPackageOutStepPageModule {}
