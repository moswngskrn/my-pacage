import { Component, OnInit, AfterViewInit, OnDestroy, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { Screenshot } from '@ionic-native/screenshot/ngx';
@Component({
  selector: 'app-admin-package-in-step3',
  templateUrl: './admin-package-in-step3.page.html',
  styleUrls: ['./admin-package-in-step3.page.scss'],
})
export class AdminPackageInStep3Page implements OnInit,OnDestroy, AfterViewInit {

  room:string;
  picture:string;
  packageNumber:string;
  date:string;
  backButtonSubscription; 
  stayHere:boolean = false;
  isSave:boolean =false;
  public myAngularxQrCode: string = null;
  constructor(
    private router:Router,
    private navCtrl:NavController,
    private route: ActivatedRoute,
    private platform: Platform,
    private ngZone:NgZone,
    public toastController: ToastController,
    private screenshot: Screenshot
  ) { 
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        var data = JSON.parse(params.special);
        console.log(data)
        this.picture = data.picture
        this.room = data.room;
        this.myAngularxQrCode = data.packageNumber;
        this.date = data.date;
      }
    })
  }

  ngOnInit() {
  }

  save(){
    this.isSave = true;
    setTimeout(()=>{
      this.screenshot.save('jpg', 80, this.myAngularxQrCode+'.jpg').then(()=>{
        this.presentToast("บันทึกรูปลงมือถึอเรียบร้อยแล้ว")
        this.isSave=false;
      }, error=>{
        this.presentToast("ผการบับันทึกผิดพลาด")
      });
    },1000)
    
  }

  ngAfterViewInit() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      if(this.stayHere){
        this.navCtrl.navigateBack('members/admin-home')
      }
    });
  }
  ngOnDestroy() { 
    this.backButtonSubscription.unsubscribe();
  }
  ionViewWillEnter(){
    this.stayHere=true;
  }

  ionViewWillLeave(){
    this.backButtonSubscription.unsubscribe();
    this.stayHere=false;
  }
  print(){
    console.log("print")
    this.navCtrl.navigateBack('members/admin-home')
  }

  async presentToast(ms) {
    const toast = await this.toastController.create({
      message: ms,
      duration: 2000
    });
    toast.present();
  }

  finish(){
    this.ngZone.run(()=>{
      this.navCtrl.navigateBack('members/admin-home')
    })
  }
}
