import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminPackageInStep3Page } from './admin-package-in-step3.page';
import {QRCodeModule} from 'angularx-qrcode'

const routes: Routes = [
  {
    path: '',
    component: AdminPackageInStep3Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QRCodeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminPackageInStep3Page]
})
export class AdminPackageInStep3PageModule {}
