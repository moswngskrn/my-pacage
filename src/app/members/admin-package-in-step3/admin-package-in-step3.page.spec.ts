import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPackageInStep3Page } from './admin-package-in-step3.page';

describe('AdminPackageInStep3Page', () => {
  let component: AdminPackageInStep3Page;
  let fixture: ComponentFixture<AdminPackageInStep3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPackageInStep3Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPackageInStep3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
