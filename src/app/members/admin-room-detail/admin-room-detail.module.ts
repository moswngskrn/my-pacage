import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminRoomDetailPage } from './admin-room-detail.page';

const routes: Routes = [
  {
    path: '',
    component: AdminRoomDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminRoomDetailPage]
})
export class AdminRoomDetailPageModule {}
