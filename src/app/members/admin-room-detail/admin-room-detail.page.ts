import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { PackageService } from 'src/app/services/package.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-admin-room-detail',
  templateUrl: './admin-room-detail.page.html',
  styleUrls: ['./admin-room-detail.page.scss'],
})
export class AdminRoomDetailPage implements OnInit {

  room: string;
  dataUser: any = []
  constructor(
    private route: ActivatedRoute,
    private packageServiec: PackageService,
    private firebaseDatabase: AngularFireDatabase,
    private router:Router
  ) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        var data = JSON.parse(params.special);
        this.room = data.room;
        this.packageServiec.getUserInRoom(data.room).on('child_added', snap => {
          if (snap.val()) {
            let users = snap.val().user;
            this.dataUser = [];
            let listAsyncPromiss = []
            for (var key in users) {
              this.dataUser.push(users[key])
              listAsyncPromiss.push(this.renderUser(users[key].userid, users[key].status))
            }
            Promise.all(listAsyncPromiss).then((data) => {
              this.dataUser=data;
            })
          }
        })
        this.packageServiec.getUserInRoom(data.room).on('child_changed', snap => {
          if (snap.val()) {
            let users = snap.val().user;
            this.dataUser = [];
            let listAsyncPromiss = []
            for (var key in users) {
              this.dataUser.push(users[key])
              listAsyncPromiss.push(this.renderUser(users[key].userid, users[key].status))
            }
            Promise.all(listAsyncPromiss).then((data) => {
              this.dataUser=data;
            })
          }
        })
      }
    });
  }

  ngOnInit() {
  }

  renderUser(userid, status) {
    return new Promise((resolve, reject) => {
      try {
        this.firebaseDatabase.database.ref('users')
          .orderByChild('userid')
          .equalTo(userid)
          .on('child_added', snap => {
            resolve({ ...snap.val(), status: status,create_at:this.timestampToDate(snap.val().create_at) })
          })
      } catch (error) {
        reject(error)
      }
    })

  }

  timestampToDate(timestamp) {
    const date = new Date(timestamp);
    const mt = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"]
    const year = date.getFullYear();
    const mount = (date.getMonth() + 1);
    const day = date.getDate();
    const h = date.getHours();
    const m = date.getSeconds();
    const s = date.getSeconds();
    return day + " " + mt[mount - 1] + " " + (year + 543) + " " + h + ":" + m;
  }

  addUser(){
    console.log("add user :",this.room)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify({room:this.room})
      }
    };
    this.router.navigate(['','members','admin-room-add-user'], navigationExtras);
  }

  changeStatus(userid){
    this.packageServiec.changeUser(this.room,userid)
  }
}
