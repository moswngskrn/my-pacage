import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PackageService } from 'src/app/services/package.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  data: any={};
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private packageService:PackageService,
    public modalCtrl: ModalController
  ) {
    this.route.queryParams.subscribe(params => {
      if (params) {
        this.data = params;
        this.packageService.readPackage(this.data.id)
      }
    });

  }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }

}
