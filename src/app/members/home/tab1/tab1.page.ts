import { Component, OnInit } from '@angular/core';
import { PackageService } from 'src/app/services/package.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {

  loadingTab1: boolean = false;
  emptyTab1: boolean = false;
  firstLoadTab1: boolean = true;
  dataTab1:any=[];

  loadingTab2: boolean = false;
  emptyTab2: boolean = false;
  firstLoadTab2: boolean = true;
  dataTab2:any=[];

  constructor(
    private packageService:PackageService,
    private router:Router
  ) { }

  ngOnInit() {
    this.loadTab1();
    this.packageService.onValueChangeIn().on('child_changed',()=>{
      console.log('child_changed')
      this.packageService.getPackageIn().then(data=>{
        this.dataTab1 = data;
      })
    })
    this.packageService.onValueChangeIn().on('child_added',snapshot=>{
      this.packageService.getPackageIn().then(data=>{
        this.dataTab1 = data;
      })
    })
  }

  // ionViewWillEnter(){
  //   console.log('ern')
  //   this.packageService.getPackageIn().then(data=>{
  //     this.dataTab1 = data;
  //   })
  // }

  loadTab1(){
    
    if (this.firstLoadTab1) {
      this.loadingTab1 = true;
      this.packageService.getPackageIn().then(data=>{
        this.loadingTab1 = false;
        this.emptyTab1 = false;
        this.firstLoadTab1=false;
        this.dataTab1 = data;
        if(!data[0]){
          this.emptyTab1=true;
        }
        // console.log(data)
      })
    }
  }

  showDetailItem(data){
    let navigationExtras: NavigationExtras = {
      queryParams: data
    };
    this.router.navigate(['','members','detail'], navigationExtras);
  }
}
