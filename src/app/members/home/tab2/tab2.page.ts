import { Component, OnInit } from '@angular/core';
import { PackageService } from 'src/app/services/package.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.page.html',
  styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {

  loadingTab1: boolean = false;
  emptyTab1: boolean = false;
  firstLoadTab1: boolean = true;
  dataTab1: any = [];

  loadingTab2: boolean = false;
  emptyTab2: boolean = false;
  firstLoadTab2: boolean = true;
  dataTab2: any = [];

  constructor(
    private packageService: PackageService,
    private router:Router
  ) { }

  ngOnInit() {
    this.loadTab1();
    this.packageService.onValueChangeOut().on('child_changed',snapshot=>{
      this.packageService.getPackageOut().then(data=>{
        this.dataTab1 = data;
        // console.log(data)
      })
    })
    this.packageService.onValueChangeOut().on('child_added',snapshot=>{
      this.packageService.getPackageOut().then(data=>{
        this.dataTab1 = data;
        // console.log(data)
      })
    })
  }

  loadTab1() {

    if (this.firstLoadTab1) {
      this.loadingTab1 = true;
      this.packageService.getPackageOut().then(data => {
        this.loadingTab1 = false;
        this.emptyTab1 = false;
        this.firstLoadTab1 = false;
        this.dataTab1 = data;
        if(!data[0]){
          this.emptyTab1=true;
        }
        // console.log(data)
      })
      // setTimeout(() => {
      //   this.loadingTab1 = false;
      //   this.emptyTab1 = false;
      //   this.firstLoadTab1=false;
      //   this.testSetDataTab1();
      // }, 3000)
    }
  }

  showDetailItem(key){
    this.packageService.getPackage(key).then(data=>{
      let navigationExtras: NavigationExtras = {
        queryParams: {
          special: JSON.stringify({...data,key:key})
        }
      };
      this.router.navigate(['','members','detail'], navigationExtras);
    })
  }

}
