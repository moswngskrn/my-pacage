import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit, NgZone } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { copyStyles } from '@angular/animations/browser/src/util';
import { Platform, ModalController, AlertController, ActionSheetController } from '@ionic/angular';
import { SuperTabs } from '@ionic-super-tabs/angular';
import { Tab1Page } from './tab1/tab1.page';
import { Tab2Page } from './tab2/tab2.page';
import { QrcodePage } from '../qrcode/qrcode.page';
import { ListPage } from '../list/list.page';
import { DetailPage } from '../detail/detail.page';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy, AfterViewInit  {
  stayHere:boolean = false;
  backButtonSubscription; 

  constructor(
    private authenticationService: AuthenticationService,
    private platform:Platform,
    public modalCtrl: ModalController,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      if(this.stayHere){
        navigator['app'].exitApp();
      }
    });
  }
  ngOnDestroy() { 
    this.backButtonSubscription.unsubscribe();
  }
  ionViewWillEnter(){
    this.stayHere=true;
  }

  ionViewWillLeave(){
    this.stayHere=false;
  }
  async logout(){
    const alert = await this.alertController.create({
      header: 'ยืนยันออกจากระบบ',
      mode:'md',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            console.log('Confirm Okay');
            this.authenticationService.logout();
          }
        }
      ]
    });

    await alert.present();
  }

  async more(){
    const modal = await this.modalCtrl.create({
      component:ListPage,
      componentProps:{}
    })
    modal.present();
  }

  async detail(){
    const modal = await this.modalCtrl.create({
      component:DetailPage,
      componentProps:{}
    })
    modal.present();
  }

  async qrcode(){
    const modal = await this.modalCtrl.create({
      component:QrcodePage,
      componentProps:{}
    })
    modal.present();
  }

  async filter() {
    const actionSheet = await this.actionSheetController.create({
      header: 'ตัวกรอง',
      mode:'md',
      buttons: [{
        text: 'ทั้งหมด',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'ยังไม่ได้รับ',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'รับแล้ว',
        handler: () => {
          console.log('Play clicked');
        }
      }, {
        text: 'ยังไม่ได้ดู',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'เลิกทำ',
        role: 'cancel',
        handler: () => {
          console.log('เลิกทำ');
        }
      }]
    });
    await actionSheet.present();
  }
}
