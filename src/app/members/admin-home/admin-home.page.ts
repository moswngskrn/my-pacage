import { Component, OnInit, OnDestroy, AfterViewInit, NgZone } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { AdminPackageInPage } from '../admin-package-in/admin-package-in.page';
import { AdminPackageOutPage } from '../admin-package-out/admin-package-out.page';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.page.html',
  styleUrls: ['./admin-home.page.scss'],
})
export class AdminHomePage implements OnInit, OnDestroy, AfterViewInit {

  stayHere:boolean = false;
  backButtonSubscription; 
  constructor(
    public modalCtrl: ModalController,
    private authenticationService:AuthenticationService,
    private platform: Platform,
    private router: Router,
  ) { }

  ngOnInit() {
    console.log("NgZone",NgZone.isInAngularZone());
  }

  ngAfterViewInit() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      if(this.stayHere){
        navigator['app'].exitApp();
      }
    });
  }
  ngOnDestroy() { 
    this.backButtonSubscription.unsubscribe();
  }

  ionViewWillEnter(){
    this.stayHere=true;
  }

  ionViewWillLeave(){
    this.stayHere=false;
  }

  packageIn(){
    this.router.navigate(['members', 'admin-package-in']);
  }
  packageOut(){
    this.router.navigate(['members', 'admin-package-out']);
  }

  packageHistory(){

  }

  async logout(){
    this.authenticationService.logout();
  }
}
