import { Component, OnInit, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { PackageService } from 'src/app/services/package.service';



@Component({
  selector: 'app-admin-package-out-step3',
  templateUrl: './admin-package-out-step3.page.html',
  styleUrls: ['./admin-package-out-step3.page.scss'],
})
export class AdminPackageOutStep3Page implements OnInit {

  @ViewChild(SignaturePad) public signaturePad: SignaturePad;
  @ViewChild('content') private content: any;
  public signaturePadOptions: Object;
  public signatureImage: string;
  list:any = []
  constructor(
    private navCtrl:NavController,
    private route: ActivatedRoute,
    private packageService:PackageService,
    public alertController: AlertController,
    public loadingController: LoadingController,
  ) { 
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.list = JSON.parse(params.special).data
      }
    });
  }

  ngOnInit() {
    this.signaturePadOptions= { // Check out https://github.com/szimek/signature_pad
      'minWidth': 1,
      'canvasWidth': this.content.el.clientWidth,
      'canvasHeight': this.content.el.clientHeight,
      'backgroundColor': '#ffffff',
      'penColor': '#000000'
    };
    console.log(this.content.el.clientWidth,this.content.el.clientHeight)
  }
  finish() {

  }

  drawCancel() {
    // this.navCtrl.push(HomePage);
  }

  async drawComplete() {
    const loading = await this.loadingController.create({
      message: 'กำลังอัพโหลดข้อมูล',
    });
    await loading.present();
    this.signatureImage = this.signaturePad.toDataURL();
    this.packageService.uploadFile2(this.signatureImage).then((url)=>{
      this.packageService.packageOut(this.list,url).then(()=>{
        loading.dismiss();
        this.navCtrl.navigateBack('members/admin-home');
      },error=>{
        loading.dismiss();
        this.presentAlert("เกิดช้อผิดพลาดโปรดลองใหม่ภายหลัง")
      })
    },error=>{
      loading.dismiss();
      this.presentAlert("ไม่สามารถบันทึกลายเซ็นได้โปรดลองใหม่อีกครั้ง")
    })
    // this.navCtrl.push(HomePage, {signatureImage: this.signatureImage});
  }

  // async presentToast(ms) {
  //   const toast = await this.toastController.create({
  //     message: ms,
  //     duration: 2000
  //   });
  //   toast.present();
  // }
  async presentAlert(m) {
    const alert = await this.alertController.create({
      header: 'เกิดข้อผิดพลาด',
      message: m,
      buttons: ['ตกลง']
    });

    await alert.present();
  }
  drawClear() {
    this.signaturePad.clear();
  }
}
