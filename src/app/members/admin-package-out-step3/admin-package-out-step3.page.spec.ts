import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPackageOutStep3Page } from './admin-package-out-step3.page';

describe('AdminPackageOutStep3Page', () => {
  let component: AdminPackageOutStep3Page;
  let fixture: ComponentFixture<AdminPackageOutStep3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPackageOutStep3Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPackageOutStep3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
