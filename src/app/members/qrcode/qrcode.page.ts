import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.page.html',
  styleUrls: ['./qrcode.page.scss'],
})
export class QrcodePage implements OnInit {

  public myAngularxQrCode: string = null;
  constructor(
    public modalCtrl: ModalController,
    private firebase:AngularFireDatabase
  ) { 
    this.myAngularxQrCode = localStorage.getItem('room')
    
  }

  ngOnInit() {
  }

  closeModal(){
    this.modalCtrl.dismiss();
  }

}
