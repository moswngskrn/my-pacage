import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {
  noti:boolean;
  room:string;
  username:string;
  constructor(
    private storage: Storage,
    public alertController: AlertController,
    private authenticationService:AuthenticationService,
    private firebase:AngularFireDatabase
  ) { 
    this.firebase.database.ref('users').orderByChild('userid').equalTo(localStorage.getItem('userid'))
    .on('child_added',snapshot=>{
      let data = snapshot.val();
      this.room = data.room;
      this.username = data.username;
      console.log(data)
    })
  }

  ngOnInit() {
    this.storage.get('notification').then(val=>{
      if(val=="on"){
        console.log("on")
        this.noti=true;
      }else{
        console.log("off")
        this.noti=false;
      }
    })
  }

  // notification(){
  //   if(this.noti){
  //     this.storage.set('notification','off')
  //   }else{
  //     this.storage.set('notification','on')
  //   }
  //   this.noti=!this.noti;
  // }
  notify() {
    console.log("notify:",this.noti)
    if(!this.noti){
      console.log("select off");
      this.storage.set('notification','off')
    }else{
      console.log("select on");
      this.storage.set('notification','on')
    }
  }

  async logout(){
    const alert = await this.alertController.create({
      header: 'ยืนยันออกจากระบบ',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            console.log('Confirm Okay');
            this.authenticationService.logout();
          }
        }
      ]
    });

    await alert.present();
  }
}
