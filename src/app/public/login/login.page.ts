import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import {trigger,transition,style,animate} from '@angular/animations';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController, LoadingController } from '@ionic/angular';
import * as html2canvas from 'html2canvas';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  animations: [
      trigger(
        'myAnimation2',
        [
          transition(':enter',
            [style({ 'opacity': 0 }),
            animate('300ms', style({'opacity': 1 }))]),
          transition(':leave',
            [style({ 'opacity': 1 }),
            animate('300ms', style({'opacity': 0 }))])
        ])
    ]
})
export class LoginPage implements OnInit {
  room:string="";
  password:string="";
  isLoading:boolean=false;

  constructor(
    private authenticationService:AuthenticationService,
    private fia:AngularFireDatabase,
    private fiah:AngularFireAuth,
    public alertController: AlertController,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    
  }

  // test(){
  //   this.authenticationService.create();
  // }
  // test2(){
  //   this.authenticationService.test();
  // }


  async login(){
    const loading = await this.loadingController.create({
      spinner:"bubbles",
      cssClass:"loading",
      mode:'ios'
    });
    await loading.present();
    this.authenticationService.login(this.room,this.password)
    .then(result=>{
      console.log(result)
      if(result){
        this.password=""
        loading.dismiss();
      }else{
        this.presentError('ระบบขัดช้องกรุณาเริ่มแอพใหม่');
        loading.dismiss();
      }
    }).catch(err=>{
      this.presentError('ไม่สามารถเข้าสู่ระบบได้ เลขห้อง หรือ รหัสผ่าน ไม่ถูกต้อง โปรดลองใหม่อีกครั้ง');
      loading.dismiss();
    })
    if(this.room!=""&&this.password!=""){
      this.isLoading=true;
     
    }
  }


  async presentError(mg) {
    const alert = await this.alertController.create({
      header: 'เกิดข้อผิดพลาด',
      message: mg,
      buttons: ['OK']
    });

    await alert.present();
  }
}
